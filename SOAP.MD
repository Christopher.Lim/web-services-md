# SOAP
What are SOAP Web Services?</br>
- Simple Object Access Protocol
- All information/message exchange happens over a common format: XML
- XML messages have a defined structure: SOAP MESSAGE
- SOAP MESSAGE consist of: </br>
Envelope </br>
Header</br>
Body</br>

### Syntax Rules
 - Encoded using XML
 - SOAP Envelope namespace
 - Not contain a DTD reference
 - Not contain XML Processing Instructions
 </br>


### SOAP Request Example
```xml
POST /InStock HTTP/1.1
Host: www.example.org
Content-Type: application/soap+xml; charset=utf-8
Content-Length: nnn

<?xml version="1.0"?>

<soap:Envelope
xmlns:soap="http://www.w3.org/2003/05/soap-envelope/"
soap:encodingStyle="http://www.w3.org/2003/05/soap-encoding">

<soap:Body xmlns:m="http://www.example.org/stock">
  <m:GetStockPrice>
    <m:StockName>IBM</m:StockName>
  </m:GetStockPrice>
</soap:Body>

</soap:Envelope>
```

### SOAP Response Example
```xml
HTTP/1.1 200 OK
Content-Type: application/soap+xml; charset=utf-8
Content-Length: nnn

<?xml version="1.0"?>

<soap:Envelope
xmlns:soap="http://www.w3.org/2003/05/soap-envelope/"
soap:encodingStyle="http://www.w3.org/2003/05/soap-encoding">

<soap:Body xmlns:m="http://www.example.org/stock">
  <m:GetStockPriceResponse>
    <m:Price>34.5</m:Price>
  </m:GetStockPriceResponse>
</soap:Body>

</soap:Envelope>
```